FROM python:3.9
WORKDIR /app
COPY . /app/
RUN pip install Flask && pip install -r requirements.txt
ENV FLASK_APP=app.py
EXPOSE 80
CMD ["flask", "run", "--host=0.0.0.0"]
